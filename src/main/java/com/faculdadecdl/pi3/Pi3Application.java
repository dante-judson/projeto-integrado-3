package com.faculdadecdl.pi3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.faculdadecdl.pi3.repository"}, repositoryImplementationPostfix = "CustomImpl")
@EntityScan(basePackages = {"com.faculdadecdl.pi3.entity"})
public class Pi3Application {

	public static void main(String[] args) {
		SpringApplication.run(Pi3Application.class, args);
	}

}

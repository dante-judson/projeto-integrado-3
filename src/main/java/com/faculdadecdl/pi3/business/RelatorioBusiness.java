package com.faculdadecdl.pi3.business;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import com.faculdadecdl.pi3.business.exception.RelatorioBusinessException;
import com.faculdadecdl.pi3.dto.AgendamentoDTO;
import com.faculdadecdl.pi3.entity.Agendamento;
import com.faculdadecdl.pi3.entity.Funcionario;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class RelatorioBusiness {

	@Autowired
	private AgendamentoBusiness agendamentoBusiness;
	
	
	@Autowired
	private ResourceLoader loader;
	
	public List<Agendamento> gerarRelatorio(AgendamentoDTO agendamentoDTO) {
		return this.agendamentoBusiness.gerarRelatorio(agendamentoDTO);
	}
	
	public byte[] exportPDF(AgendamentoDTO filtro, Funcionario usuarioLogado) throws RelatorioBusinessException {
		byte[] reportData = null;
		try {
			JRDataSource datasource = this.createDataSource(this.gerarRelatorio(filtro));
			InputStream jasperInputStream = loader.getResource("classpath:static/assets/jasper/relatorio.jasper").getInputStream();
			JasperPrint printer = JasperFillManager.fillReport(jasperInputStream, 
					this.generateParameters(usuarioLogado, filtro, datasource),
					new JREmptyDataSource());
			
			reportData = JasperExportManager.exportReportToPdf(printer);
			
		} catch (JRException e) {
			e.printStackTrace();
			throw new RelatorioBusinessException(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reportData;
	}
	
	private Map<String, Object> generateParameters(Funcionario usuarioLogado, AgendamentoDTO filtro, JRDataSource datasource) { 
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("responsavelNome", filtro.getResponsavel()==null?"":filtro.getResponsavel().getNomeCompleto());
		parameters.put("responsavelAgendamentoNome", filtro.getResponsavelAgendamento()==null?"":filtro.getResponsavelAgendamento().getNomeCompleto());
		parameters.put("recursoNome", filtro.getRecurso()==null?"":filtro.getRecurso().getNome());
		parameters.put("dataAgendamentoInicio", this.dateToString(filtro.getDataInicio()));
		parameters.put("dataAgendamentoFim", this.dateToString(filtro.getDataFim()));
		parameters.put("removerCancelados",filtro.getRemoverCancelados().toString());
		parameters.put("geradoPor",usuarioLogado.getNomeCompleto());
		parameters.put("agendamentoDataSource", datasource);
		return parameters;
	}
	
	private JRDataSource createDataSource(List<Agendamento> agendamentos) {
		List<Map<String,String>> data = new ArrayList<Map<String,String>>();
		for(Agendamento agendamento : agendamentos) {
			Map<String, String> entry = new HashMap<String, String>();
			entry.put("responsavel", agendamento.getResponsavel().getNomeCompleto());
			entry.put("responsavelAgendamento", agendamento.getResponsavelAgendamento().getNomeCompleto());
			entry.put("recurso", agendamento.getRecurso().getNome());
			entry.put("dataReservado", this.dateToString(agendamento.getDataReservado()));
			entry.put("horarioInicio", this.dateToString(agendamento.getDataAgendamentoInicio()));
			entry.put("horarioFim", this.dateToString(agendamento.getDataAgendamentoInicio()));
			entry.put("motivoCancelamento", agendamento.getMotivoCancelamento());
			entry.put("dataCancelamento", this.dateToString(agendamento.getDataCancelado()));
			data.add(entry);
		}
		
		return new JRBeanCollectionDataSource(data);
	}
	
	private String dateToString(Calendar date) {
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		if (date != null) {			
			return formater.format(date.getTime());
		} else {
			return null;
		}
	}
}

package com.faculdadecdl.pi3.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.faculdadecdl.pi3.business.FuncionarioBusiness;
import com.faculdadecdl.pi3.business.exception.FuncionarioBusinessException;
import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.util.SessionUtils;

@Controller
public class LoginController {

	@Autowired
	private FuncionarioBusiness funcionarioBusiness;
	
	@GetMapping("/")
	public String loginForm(Model model) {
		model.addAttribute("funcionario", new Funcionario());
		return "login";
	}
	
	@PostMapping("/login")
	public String login(@ModelAttribute Funcionario funcionario, Model model, HttpSession session) {
		Funcionario funcionarioLogado = new Funcionario();
		try {			
			funcionarioLogado = funcionarioBusiness.autenticar(funcionario);
		} catch (FuncionarioBusinessException e) {
			model.addAttribute("errors",e.getMessage());
			return "login";
		}
		
		SessionUtils.setUsuarioLogado(funcionarioLogado, session);
//		session.setAttribute("funcionarioLogado", funcionarioLogado);
		return "redirect:app/agendamento";
	}
	
	@GetMapping("/create-account")
	public String createAccountPage(Model model) {
		model.addAttribute("funcionario", new Funcionario());
		return "create-account";
	}
	
	@PostMapping("/create-account")
	public String createAccount(@ModelAttribute @Valid Funcionario funcionario, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {	
			return "create-account";
		} else {
			try {
				this.funcionarioBusiness.criar(funcionario);
				model.addAttribute("msg", "Conta criada com sucesso");
				return "login";
			} catch (FuncionarioBusinessException e) {
				model.addAttribute("errors",e.getMessage());
				return "create-account";
			}
		}	
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		SessionUtils.logout(session);
		return "redirect:/";
	}
}

package com.faculdadecdl.pi3.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.faculdadecdl.pi3.business.AgendamentoBusiness;
import com.faculdadecdl.pi3.business.exception.AgendamentoBusinessException;
import com.faculdadecdl.pi3.entity.Agendamento;
import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.util.SessionUtils;

@Controller
@RequestMapping("app/agendamento")
public class AgendamentoController {

	@Autowired
	private AgendamentoBusiness agendamentoBusiness;
	
	@GetMapping("")
	public String meusAgendamentos(Model model, HttpSession session) {
		model.addAttribute("title", "Meus agendamentos");
		Funcionario funcionarioLogado = SessionUtils.getUsuarioLogado(session);
		model.addAttribute("agendadosPorMim", agendamentoBusiness.findByFuncionarioResponsavelAgendamento(funcionarioLogado));
		model.addAttribute("agendadosParaMim", agendamentoBusiness.findByFuncionarioResponsavel(funcionarioLogado));
		return "agendamento/list";
	}
	
	@GetMapping("adicionar")
	public String adicionarForm(Model model) {
		model.addAttribute("title", "Novo agendamento");
		model.addAttribute("agendamento", new Agendamento());
		return "agendamento/adicionar";
	}
	
	@PostMapping("adicionar") 
	public String adicionar(RedirectAttributes ra, @ModelAttribute Agendamento agendamento, Model model, HttpSession session) {
		try {			
			agendamentoBusiness.criar(agendamento, SessionUtils.getUsuarioLogado(session));
		} catch (AgendamentoBusinessException e) {
			model.addAttribute("errors", e.getMessage());
			return "agendamento/adicionar";
		}
		ra.addFlashAttribute("msg", "Agendamento criado com sucesso!");
		return "redirect:/app/agendamento";
	}
	
	@GetMapping("cancelar/{id}")
	public String cancelar(Model model, RedirectAttributes ra, @RequestParam String motivoCancelamento, @PathVariable Long id) {
		try {
			this.agendamentoBusiness.cancela(motivoCancelamento, id);
			ra.addFlashAttribute("msg", "Agendamento Cancelado com sucesso!");
		} catch (AgendamentoBusinessException e) {
			model.addAttribute("errors", e.getMessage());
		}
		return "redirect:/app/agendamento";
	}
	
}

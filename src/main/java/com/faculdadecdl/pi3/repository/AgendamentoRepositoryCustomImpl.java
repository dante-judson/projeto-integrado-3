package com.faculdadecdl.pi3.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.faculdadecdl.pi3.dto.AgendamentoDTO;
import com.faculdadecdl.pi3.entity.Agendamento;
import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.entity.Recurso;

public class AgendamentoRepositoryCustomImpl implements AgendamentoRepositoryCustom {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public List<Agendamento> generateReport(AgendamentoDTO agendamentoDTO) {
		CriteriaBuilder cb = this.manager.getCriteriaBuilder();
		
		CriteriaQuery<Agendamento> query = cb.createQuery(Agendamento.class);
		Root<Agendamento> agendamento = query.from(Agendamento.class);
		
		Path<Long> responsavelIdPath = agendamento.get("responsavel");
		Path<Long> responsavelAgendamentoIdPath = agendamento.get("responsavelAgendamento");
		Path<Long> recursoIdPath = agendamento.get("recurso");
		Path<Calendar> dataAgendamentoInicioPath = agendamento.get("dataAgendamentoInicio");
		Path<Calendar> dataAgendamentoFimPath = agendamento.get("dataAgendamentoFim");
		Path<String> motivoCancelamentoPath = agendamento.get("motivoCancelamento");
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		
		if (agendamentoDTO.getResponsavel() != null && agendamentoDTO.getResponsavel().getId() != null) {
			predicates.add(cb.equal(responsavelIdPath, new Funcionario(agendamentoDTO.getResponsavel().getId())));
		}
		
		if (agendamentoDTO.getResponsavelAgendamento() != null && agendamentoDTO.getResponsavelAgendamento().getId() != null) {
			predicates.add(cb.equal(responsavelAgendamentoIdPath, new Funcionario(agendamentoDTO.getResponsavelAgendamento().getId())));
		}
		
		if(agendamentoDTO.getRecurso() != null && agendamentoDTO.getRecurso().getId() != null) {
			predicates.add(cb.equal(recursoIdPath, new Recurso(agendamentoDTO.getRecurso().getId())));
		}
		
		if (agendamentoDTO.getDataInicio() != null) {
			predicates.add(cb.greaterThanOrEqualTo(dataAgendamentoInicioPath, agendamentoDTO.getDataInicio()));
		}
		
		if (agendamentoDTO.getDataFim() != null) {
			predicates.add(cb.lessThanOrEqualTo(dataAgendamentoFimPath, agendamentoDTO.getDataFim()));
		}
		
		if (agendamentoDTO.getRemoverCancelados()) {
			predicates.add(cb.isNull(motivoCancelamentoPath));
		}
		
		query.select(agendamento)
		.where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
		
		return this.manager.createQuery(query).getResultList();
	}

}

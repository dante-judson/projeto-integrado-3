package com.faculdadecdl.pi3.business;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.faculdadecdl.pi3.business.exception.AgendamentoBusinessException;
import com.faculdadecdl.pi3.dto.AgendamentoDTO;
import com.faculdadecdl.pi3.entity.Agendamento;
import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.repository.AgendamentoRepository;

@Component
public class AgendamentoBusiness {

	@Autowired
	private AgendamentoRepository agendamentoRepository; 
	
	public List<Agendamento> findByFuncionarioResponsavelAgendamento(Funcionario funcionario) {
		return this
				.agendamentoRepository
				.findByResponsavelAgendamentoAndDataAgendamentoInicioGreaterThanEqualAndDataCanceladoIsNull(funcionario, Calendar.getInstance());
	}
	
	public List<Agendamento> findByFuncionarioResponsavel(Funcionario funcionario) {
		return this
				.agendamentoRepository
				.findByResponsavelAndDataAgendamentoInicioGreaterThanEqualAndDataCanceladoIsNull(funcionario, Calendar.getInstance());
	}
	
	private void validaDatas(Agendamento agendamento) throws AgendamentoBusinessException {
		if (agendamento.getDataAgendamentoInicio().before(Calendar.getInstance())) {
			throw new AgendamentoBusinessException("Data de inicio de agendamento deve ser futura!");
		}
		
		if (agendamento.getDataAgendamentoInicio().after(agendamento.getDataAgendamentoFim())) {
			throw new AgendamentoBusinessException("Data de fim de agendamento deve ser maior que a data de inicio!");
		}
	}
	
	private void validaRecursoDisponível(Agendamento agendamento) throws AgendamentoBusinessException {
		Calendar inicio = agendamento.getDataAgendamentoInicio();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		List<Agendamento> agendamentosEncontrados = this.agendamentoRepository
				.findByDataAgendamentoInicioLessThanEqualAndDataAgendamentoFimGreaterThanEqualAndRecursoAndDataCanceladoIsNull(inicio, inicio, agendamento.getRecurso());
		if (agendamentosEncontrados.size() > 0) {
			Agendamento agendado = agendamentosEncontrados.get(0);
			throw new AgendamentoBusinessException("Recurso não disponível para data agendada. Já existe um agendamento de "
					+ format.format(agendado.getDataAgendamentoInicio().getTime()) + " até " +
					format.format(agendado.getDataAgendamentoFim().getTime()));
		}
		
		Calendar fim = agendamento.getDataAgendamentoFim();
		agendamentosEncontrados = this.agendamentoRepository
				.findByDataAgendamentoInicioLessThanEqualAndDataAgendamentoFimGreaterThanEqualAndRecursoAndDataCanceladoIsNull(fim, fim, agendamento.getRecurso());
		
		if (agendamentosEncontrados.size() > 0) {
			Agendamento agendado = agendamentosEncontrados.get(0);
			throw new AgendamentoBusinessException("Recurso não disponível para data agendada. Já existe um agendamento de "
					+ format.format(agendado.getDataAgendamentoInicio().getTime()) + " até " +
					format.format(agendado.getDataAgendamentoFim().getTime()));
			}
	}
	
	public Agendamento criar(Agendamento agendamento, Funcionario responsavelAgendamento) throws AgendamentoBusinessException {
		this.validaDatas(agendamento);
		
		this.validaRecursoDisponível(agendamento);
		
		agendamento.setResponsavelAgendamento(responsavelAgendamento);
		agendamento.setDataReservado(Calendar.getInstance());
		return agendamentoRepository.save(agendamento);
	}
	
	public Agendamento findAtendimentoById(Long id) throws AgendamentoBusinessException {
		Optional<Agendamento> op = this.agendamentoRepository.findById(id);
		op.orElseThrow(() -> new AgendamentoBusinessException("Agendamento não encontrado"));
		
		return op.get();
	}
	
	public void cancela(String motivoCancelamento, Long id) throws AgendamentoBusinessException {
		Agendamento agendamento = this.findAtendimentoById(id);
		agendamento.setMotivoCancelamento(motivoCancelamento);
		agendamento.setDataCancelado(Calendar.getInstance());
		this.agendamentoRepository.save(agendamento);
	}
	
	public List<Agendamento> gerarRelatorio(AgendamentoDTO agendamentoDTO) {
		return this.agendamentoRepository.generateReport(agendamentoDTO);
	}
}

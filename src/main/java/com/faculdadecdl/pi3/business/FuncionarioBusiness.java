package com.faculdadecdl.pi3.business;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.faculdadecdl.pi3.business.exception.FuncionarioBusinessException;
import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.entity.enums.Status;
import com.faculdadecdl.pi3.repository.FuncionarioRepository;

@Service
public class FuncionarioBusiness {

	@Autowired
	private FuncionarioRepository repository;

	public Funcionario criar(Funcionario funcionario) throws FuncionarioBusinessException {
		if (!funcionario.getSenha().equals(funcionario.getConfirmarSenha())) {
			throw new FuncionarioBusinessException("Senhas informadas não conferem");
		} else {
			this.validaUniqueConstraints(funcionario);
			funcionario.setSenha(this.encriptaSenha(funcionario.getSenha()));
			return repository.save(funcionario);
		}
	}
	
	public List<Funcionario> findAll() {
		List<Funcionario> funcionarios = this.repository.findAll();
		return funcionarios.stream()
		.map(f -> {
			f.setSenha(null);
			return f;
		}).collect(Collectors.toList());
	}

	private void validaUniqueConstraints(Funcionario funcionario) throws FuncionarioBusinessException {

		this.repository.findByEmail(funcionario.getEmail()).ifPresent(value -> {
			if (funcionario.getId() == null || !funcionario.getId().equals(value.getId())) {				
				throw new FuncionarioBusinessException("Email já cadastrado na nossa base, favor utilizar outro email");
			}
		});

		this.repository.findByCpf(funcionario.getCpf()).ifPresent(value -> {
			if (funcionario.getId() == null || !funcionario.getId().equals(value.getId())) {				
				throw new FuncionarioBusinessException("CPF já cadastrado na nossa base, favor utilizar outro CPF");
			}
		});

		this.repository.findByUsuario(funcionario.getUsuario()).ifPresent(value -> {
			if (funcionario.getId() == null || !funcionario.getId().equals(value.getId())) {				
				throw new FuncionarioBusinessException("Usuário já cadastrado na nossa base, favor utilizar outro usuário");
			}
		});

	}

	private String encriptaSenha(String senha) {
		String salt = BCrypt.gensalt();
		return BCrypt.hashpw(senha, salt);
	}

	private boolean validaSenha(String senha, String encryptedSenha) {
		return BCrypt.checkpw(senha, encryptedSenha);
	}

	public Funcionario autenticar(Funcionario funcionario) throws FuncionarioBusinessException {
		
		Optional<Funcionario> op = repository.findByUsuario(funcionario.getUsuario());
		
		op.ifPresentOrElse(func -> {
			String encryptedSenha = func.getSenha();
			if (!validaSenha(funcionario.getSenha(), encryptedSenha)) {
				throw new FuncionarioBusinessException("Usuário ou senha inválida.");
			}
		}, () -> {throw new FuncionarioBusinessException("Usuário ou senha inválida.");});
		
		Funcionario funcionarioLogado =  op.get();
		
		if(!funcionarioLogado.getStatus().equals(Status.ATIVO)) {
			throw new FuncionarioBusinessException("Usuário não está ativo no sistema");
		}
		
		funcionarioLogado.setSenha(null);
		return funcionarioLogado;
	}
	
	public Funcionario findById(Long id) throws FuncionarioBusinessException {
		Optional<Funcionario> op = this.repository.findById(id);
		op.orElseThrow(() -> new FuncionarioBusinessException("Funcioário não encontrado"));
		return op.get();
	}
	
	public void deletaById(Long id) throws FuncionarioBusinessException {
		Funcionario funcionario = this.findById(id);
		this.repository.delete(funcionario);
	}
	
	public List<Funcionario> buscaAtivoPorNomeCompletoOuCpf(String filtro, boolean incluirInativos) {
		if (incluirInativos) {
			return this.repository.findByNomeCompletoOrCpf(filtro);
		} else {			
			return this.repository.findByNomeCompletoOrCpfAndIsActive(filtro);
		}
	}
}

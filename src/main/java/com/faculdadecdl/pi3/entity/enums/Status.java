package com.faculdadecdl.pi3.entity.enums;

public enum Status {
	
	ATIVO("Ativo",'a'),
	INATIVO("Inativo",'i'),
	SUSPENSO("Suspenso",'s');
	
	public String displayValue;
	public char databaseValue;
	
	Status(String displayValue, char databaseValue) {
		this.databaseValue = databaseValue;
		this.displayValue = displayValue;
	}
	
	public Status getEnumByDatabaseValue(char databaseValue) {
		Status[] statuses = Status.values();
		
		for (Status status : statuses) {
			if (status.databaseValue == databaseValue) {
				return status;
			}
		}
		
		throw new EnumConstantNotPresentException(Status.class, "databaseValue");
	}
	
	
	
}

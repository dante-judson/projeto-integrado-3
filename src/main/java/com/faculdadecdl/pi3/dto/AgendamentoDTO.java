package com.faculdadecdl.pi3.dto;

import java.util.Calendar;

import org.springframework.format.annotation.DateTimeFormat;

import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.entity.Recurso;

public class AgendamentoDTO {
	
	private Funcionario responsavel;
	
	private Funcionario responsavelAgendamento;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Calendar dataInicio;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Calendar dataFim;
	
	private Recurso recurso;
	
	private Boolean removerCancelados;
	
	public Recurso getRecurso() {
		return recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}

	public Funcionario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}

	public Funcionario getResponsavelAgendamento() {
		return responsavelAgendamento;
	}

	public void setResponsavelAgendamento(Funcionario responsavelAgendamento) {
		this.responsavelAgendamento = responsavelAgendamento;
	}

	public Calendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Calendar getDataFim() {
		return dataFim;
	}

	public void setDataFim(Calendar dataFim) {
		this.dataFim = dataFim;
	}

	public Boolean getRemoverCancelados() {
		return removerCancelados;
	}

	public void setRemoverCancelados(Boolean removerCancelados) {
		this.removerCancelados = removerCancelados;
	}
	
}

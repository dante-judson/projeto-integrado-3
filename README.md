# Projeto Integrado III
## Facudade CDL

Curso: Análise e Desenvolvimento de Sistemas<br/>
Turno: Noite<br/>
Equipe:<br/>
Dante Judson Gomes de Lima : 2020011734<br/>
Ingrid Rodrigues Monteiro da Silva Tavares : 2019010466<br/>
Jane Vieira Pinto : 2019010371<br/>


### Softwares Necessários
* Java JDK 11
* Maven
* Banco de dados Mysql
* GIT

### Instalação do software

* Clonar o codigo deste repositório
* Abrir o arquivo application.properties:<br/>
`/src/main/resources/application.properties`
* Altere as propriedades abaixo de acordo com sua conexão de banco:<br/>
`spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/projeto3`   <br/>
`spring.datasource.username=root` <br/>
`spring.datasource.password=1234`<br/>
Exemplo: Nesse caso o banco está configurado para o host localhost, porta 3306, e o nome do banco projeto3 o usuario é root e a senha 1234

* Abra o prompt de comando, no Windows, ou terminal, Linux e navegue até a pasta raiz do projeto.
* Execute o comando<br/>
`mvnw clean install`
e o Maven será encarregado de baixar e compilar todas as dependências do projeto. 
* Após terminar o processo de instalação do projeto haverá um arquivo **PI3-0.0.1-SNAPSHOT.jar** dentro da pasta **target**. 
* Navegue com o terminal ou prompt até a pasta **target**.
* Execute o comando:<br/>
`java -jar PI3-0.0.1-SNAPSHOT.jar`
e o projeto irá rodar e poderá ser acessado em:<br/>
`http://localhost:8080`

### Banco de Dados
A aplicação no primeiro boot irá criar todas as tabelas e relacionamentos necessários para funcionar, dessa forma não será necessário um arquivo de dump para configuração inicial do projeto.

# Informações importantes
Link do software já em execução:<br/>
[https://projeto-pi3.herokuapp.com/](https://projeto-pi3.herokuapp.com/)

Link do vídeo demonstrativo:<br/>
[https://www.youtube.com/watch?v=dWa5eHxg7Hc](https://www.youtube.com/watch?v=dWa5eHxg7Hc)

package com.faculdadecdl.pi3.util;

import javax.servlet.http.HttpSession;

import com.faculdadecdl.pi3.entity.Funcionario;

public class SessionUtils {

	public static Funcionario getUsuarioLogado(HttpSession session) {
		return (Funcionario) session.getAttribute("funcionarioLogado");
	}
	
	public static void logout(HttpSession session) {
		session.invalidate();
	}
	
	public static void setUsuarioLogado(Funcionario funcionario, HttpSession session) {
		session.setAttribute("funcionarioLogado", funcionario);
	}
	
}

package com.faculdadecdl.pi3.repository;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.faculdadecdl.pi3.entity.Agendamento;
import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.entity.Recurso;

public interface AgendamentoRepository extends JpaRepository<Agendamento, Long>, AgendamentoRepositoryCustom{
	
	public List<Agendamento> findByResponsavelAgendamentoAndDataAgendamentoInicioGreaterThanEqualAndDataCanceladoIsNull
	(Funcionario responsavelAgendamento, Calendar dataAgendamentoInicio);
	
	public List<Agendamento> findByResponsavelAndDataAgendamentoInicioGreaterThanEqualAndDataCanceladoIsNull
	(Funcionario responsavel, Calendar dataAgendamentoInicio);
	
	public List<Agendamento> findByDataAgendamentoInicioLessThanEqualAndDataAgendamentoFimGreaterThanEqualAndRecursoAndDataCanceladoIsNull
	(Calendar dataAgendamentoInicio, Calendar dataAgendamentoFim, Recurso recurso);
	
}

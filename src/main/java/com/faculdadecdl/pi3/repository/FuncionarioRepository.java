package com.faculdadecdl.pi3.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.faculdadecdl.pi3.entity.Funcionario;

public interface FuncionarioRepository extends JpaRepository<Funcionario, Long>{

	public Optional<Funcionario> findByCpf(String cpf);
	
	public Optional<Funcionario> findByEmail(String email);
	
	public Optional<Funcionario> findByUsuario(String usuario);
	
	@Query(value = "SELECT f from Funcionario f where "
			+ "(lower(f.nomeCompleto) like lower(concat('%',?1,'%')) or lower(f.cpf) like lower(concat('%',?1,'%'))) "
			+ "and f.status = com.faculdadecdl.pi3.entity.enums.Status.ATIVO")
	public List<Funcionario> findByNomeCompletoOrCpfAndIsActive(String filtro);

	@Query(value = "SELECT f from Funcionario f where "
			+ "(lower(f.nomeCompleto) like lower(concat('%',?1,'%')) or lower(f.cpf) like lower(concat('%',?1,'%'))) ")
	public List<Funcionario> findByNomeCompletoOrCpf(String filtro);
}

package com.faculdadecdl.pi3.business.exception;

public class FuncionarioBusinessException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6279880139046205824L;
	
	public FuncionarioBusinessException(String msg) {
		super(msg);
	}

}

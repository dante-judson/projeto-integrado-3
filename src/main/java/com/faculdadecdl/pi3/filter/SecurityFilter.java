package com.faculdadecdl.pi3.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.faculdadecdl.pi3.entity.Funcionario;
import com.faculdadecdl.pi3.util.SessionUtils;

@Component
public class SecurityFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		Funcionario funcionarioLogado = SessionUtils.getUsuarioLogado(httpRequest.getSession());
		if (httpRequest.getRequestURI().contains("app") && (funcionarioLogado == null)) {
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.sendRedirect("/");
			return;
		}
		
		chain.doFilter(request, response);		
	}

}

package com.faculdadecdl.pi3.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.faculdadecdl.pi3.business.RelatorioBusiness;
import com.faculdadecdl.pi3.business.exception.RelatorioBusinessException;
import com.faculdadecdl.pi3.dto.AgendamentoDTO;
import com.faculdadecdl.pi3.util.SessionUtils;

@Controller
@RequestMapping("app/relatorio")
public class RelatorioController {

	@Autowired
	private RelatorioBusiness relatorioBusiness;
	
	@GetMapping("")
	public String relatorioForm(Model model) {
		model.addAttribute("title", "Relatórios");
		model.addAttribute("agendamentoFilter", new AgendamentoDTO());
		return "relatorio/list";
	}
	
	@GetMapping("gerar")
	public String gerarRelatorio(@ModelAttribute AgendamentoDTO agendamentoDTO, Model model) {
		model.addAttribute("agendamentoFilter", agendamentoDTO);
		model.addAttribute("resultado", this.relatorioBusiness.gerarRelatorio(agendamentoDTO));
		return "relatorio/list";
	}
	
	@GetMapping("exportar")
	public void exportarRelatorio(@ModelAttribute AgendamentoDTO agendamentoDTO, Model model, HttpServletResponse response, HttpSession session) {
		model.addAttribute("agendamentoFilter", agendamentoDTO);
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(this.relatorioBusiness.exportPDF(agendamentoDTO, SessionUtils.getUsuarioLogado(session)));
			IOUtils.copy(inputStream, response.getOutputStream());
		} catch (RelatorioBusinessException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

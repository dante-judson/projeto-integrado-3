package com.faculdadecdl.pi3.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.faculdadecdl.pi3.entity.enums.Status;

@Entity
public class Recurso {

	@Id
	@GeneratedValue
	private Long Id;
	
	@Column(unique = true, nullable = false)
	@NotEmpty(message = "O campo código não pode ser vazio")
	private String codigo;
	
	@Column(nullable = false)
	@NotEmpty(message = "O campo nome não pode ser vazio")
	private String nome;

	@Column(nullable = false)
	@NotEmpty(message = "O campo sigla não pode ser vazio")
	private String sigla;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	@NotNull(message = "O campo status não pode ser vazio")
	private Status status;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Recurso() {
	}

	public Recurso(Long id) {
		this.Id = id;
	}

}

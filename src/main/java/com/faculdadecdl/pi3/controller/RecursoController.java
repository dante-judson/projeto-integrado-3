package com.faculdadecdl.pi3.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.faculdadecdl.pi3.business.RecursoBusiness;
import com.faculdadecdl.pi3.business.exception.RecursoBusinessException;
import com.faculdadecdl.pi3.entity.Recurso;

@Controller
@RequestMapping("app/recurso")
public class RecursoController {

	@Autowired
	private RecursoBusiness recursoBusiness;
	
	@GetMapping("")
	public String listRecursos(Model model) {
		model.addAttribute("title", "Recursos");
		model.addAttribute("recursos", this.recursoBusiness.findAll());
		return "recurso/list";
	}
	
	@GetMapping("adicionar")
	public String addRecursoForm(Model model) {
		model.addAttribute("title", "Adicionar Recurso");
		model.addAttribute("recurso", new Recurso());
		return "recurso/adicionar";
	}
	
	@PostMapping("adicionar")
	public String addRecurso(Model model, @ModelAttribute @Valid Recurso recurso, BindingResult bindingResult, RedirectAttributes ra) {

		model.addAttribute("title", "Adicionar Recurso");
		if(bindingResult.hasErrors()) {
			return "recurso/adicionar";
		} else {
			try {				
				this.recursoBusiness.save(recurso);
				ra.addFlashAttribute("msg", "Recurso criado com sucesso.");
				return "redirect:/app/recurso";
			} catch (RecursoBusinessException e) {
				model.addAttribute("errors", e.getMessage());
				return "recurso/adicionar";
			}
		}
	}
	
	@GetMapping("editar/{id}")
	public String editarRecursoForm(Model model,@PathVariable(name = "id") Long id, RedirectAttributes ra) {
		try {			
			model.addAttribute("title", "Editar Recurso");
			model.addAttribute("recurso", this.recursoBusiness.findById(id));
			return "recurso/editar";
		} catch (RecursoBusinessException e) {
			ra.addFlashAttribute("errors", e.getMessage());
			return "redirect:/app/recurso";
		}
	}
	
	@PostMapping("editar")
	public String editarRecurso(Model model, RedirectAttributes ra, @ModelAttribute @Valid Recurso recurso, BindingResult bindingResults) {
		if (bindingResults.hasErrors()) {
			return "recurso/editar";
		} else {			
			try {			
				this.recursoBusiness.edit(recurso);
				ra.addFlashAttribute("msg","Recurso editado com sucesso");
				return "redirect:/app/recurso";
			} catch (RecursoBusinessException e) {
				model.addAttribute("errors", e.getMessage());
				return "recurso/editar";
			}
		}
	}
	
	@GetMapping("deletar/{id}")
	public String deletaRecursoForm(Model model,@PathVariable(name = "id") Long id, RedirectAttributes ra) {
		try {			
			this.recursoBusiness.deleteRecursoById(id);
			ra.addFlashAttribute("msg", "Recurso excluido com sucesso");
		} catch (RecursoBusinessException e) {
			ra.addFlashAttribute("errors", e.getMessage());
		}
		return "redirect:/app/recurso";
	}
	
	@GetMapping("buscar")
	@ResponseBody
	public List<Recurso> find(String filtro) {
		return this.recursoBusiness.buscaPorCodigoOuNome(filtro);
	}
}

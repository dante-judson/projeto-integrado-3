package com.faculdadecdl.pi3.repository;

import java.util.List;

import com.faculdadecdl.pi3.dto.AgendamentoDTO;
import com.faculdadecdl.pi3.entity.Agendamento;

public interface AgendamentoRepositoryCustom {
	
	public List<Agendamento> generateReport(AgendamentoDTO agendamentoDTO);

}

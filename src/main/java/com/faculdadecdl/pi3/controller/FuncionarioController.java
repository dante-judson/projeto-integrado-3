package com.faculdadecdl.pi3.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.faculdadecdl.pi3.business.FuncionarioBusiness;
import com.faculdadecdl.pi3.business.exception.FuncionarioBusinessException;
import com.faculdadecdl.pi3.entity.Funcionario;

@Controller
@RequestMapping("app/funcionario")
public class FuncionarioController {

	@Autowired
	private FuncionarioBusiness funcionarioBusiness;
	
	@GetMapping("")
	public String findAll(Model model) {
		model.addAttribute("title", "Lista de funcionários");
		model.addAttribute("funcionarios",funcionarioBusiness.findAll());
		return "funcionario/list";
	}
	
	@GetMapping("adicionar")
	public String addFuncionarioForm(Model model) {
		model.addAttribute("title", "Adicionar Funcionário");
		model.addAttribute("funcionario", new Funcionario());
		return "funcionario/adicionar";
	}
	
	
	@PostMapping("adicionar")
	public String addFuncionario(@ModelAttribute @Valid Funcionario funcionario, BindingResult bindingResult, Model model, RedirectAttributes ra) {
		if (bindingResult.hasErrors()) {	
			return "funcionario/adicionar";
		} else {
			try {
				this.funcionarioBusiness.criar(funcionario);
				ra.addFlashAttribute("msg", "Funcionário criado com sucesso");
				return "redirect:/app/funcionario";
			} catch (FuncionarioBusinessException e) {
				model.addAttribute("errors",e.getMessage());
				return "funcionario/adicionar";
			}
		}	
	}
	
	@GetMapping("editar/{id}")
	public String editarFuncionarioForm(@PathVariable(name = "id")Long id, RedirectAttributes ra, Model model) {
		try {
			model.addAttribute("funcionario", this.funcionarioBusiness.findById(id));
			model.addAttribute("title", "Editar Funcionário");
			return "funcionario/editar";
		} catch (FuncionarioBusinessException e) {
			ra.addFlashAttribute("errors", e.getMessage());
			return "redirect:/app/funcionario";
		}
	}
	
	@GetMapping("deletar/{id}")
	public String deletarFuncionarioForm(@PathVariable(name = "id")Long id, RedirectAttributes ra) {
		try {
			this.funcionarioBusiness.deletaById(id);
			ra.addFlashAttribute("msg", "Funcionário excluido com sucesso");
		} catch (FuncionarioBusinessException e) {
			ra.addFlashAttribute("errors", e.getMessage());
		}
		return "redirect:/app/funcionario";
	}
	
	@PostMapping("editar")
	public String editarFuncionario(@ModelAttribute @Valid Funcionario funcionario, BindingResult bindingResult, Model model, RedirectAttributes ra) {
		if (bindingResult.hasErrors()) {	
			return "funcionario/editar";
		} else {
			try {
				this.funcionarioBusiness.criar(funcionario);
				ra.addFlashAttribute("msg", "Funcionário editado com sucesso");
				return "redirect:/app/funcionario";
			} catch (FuncionarioBusinessException e) {
				model.addAttribute("errors",e.getMessage());
				return "funcionario/editar";
			}
		}	
	}
	
	@GetMapping(value = "buscar")
	@ResponseBody
	public List<Funcionario> find(String filtro, boolean incluirInativos) {
		return this.funcionarioBusiness.buscaAtivoPorNomeCompletoOuCpf(filtro, incluirInativos);
	}
}

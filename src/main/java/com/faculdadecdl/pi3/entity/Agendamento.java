package com.faculdadecdl.pi3.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Agendamento {

	@GeneratedValue
	@Id
	private Long Id;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Funcionario responsavel;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Funcionario responsavelAgendamento;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Recurso recurso;
	
	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Calendar dataAgendamentoInicio;

	@Column(nullable = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private Calendar dataAgendamentoFim;

	@Column(nullable = false)
	private Calendar dataReservado;
	
	private Calendar dataCancelado;
	
	private String motivoCancelamento;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Funcionario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Funcionario responsavel) {
		this.responsavel = responsavel;
	}

	public Funcionario getResponsavelAgendamento() {
		return responsavelAgendamento;
	}

	public void setResponsavelAgendamento(Funcionario responsavelAgendamento) {
		this.responsavelAgendamento = responsavelAgendamento;
	}

	public Recurso getRecurso() {
		return recurso;
	}

	public void setRecurso(Recurso recurso) {
		this.recurso = recurso;
	}

	public Calendar getDataAgendamentoInicio() {
		return dataAgendamentoInicio;
	}

	public void setDataAgendamentoInicio(Calendar dataAgendamentoInicio) {
		this.dataAgendamentoInicio = dataAgendamentoInicio;
	}

	public Calendar getDataAgendamentoFim() {
		return dataAgendamentoFim;
	}

	public void setDataAgendamentoFim(Calendar dataAgendamentoFim) {
		this.dataAgendamentoFim = dataAgendamentoFim;
	}

	public Calendar getDataReservado() {
		return dataReservado;
	}

	public void setDataReservado(Calendar dataReservado) {
		this.dataReservado = dataReservado;
	}

	public Calendar getDataCancelado() {
		return dataCancelado;
	}

	public void setDataCancelado(Calendar dataCancelado) {
		this.dataCancelado = dataCancelado;
	}

	public String getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}
	
	
	
}

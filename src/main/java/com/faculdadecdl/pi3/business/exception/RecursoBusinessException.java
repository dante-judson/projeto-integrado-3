package com.faculdadecdl.pi3.business.exception;

public class RecursoBusinessException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7655247168876994062L;
	
	public RecursoBusinessException(String msg) {
		super(msg);
	}

}

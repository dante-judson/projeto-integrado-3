package com.faculdadecdl.pi3.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.faculdadecdl.pi3.entity.Recurso;
import com.faculdadecdl.pi3.entity.enums.Status;

public interface RecursoRepository extends JpaRepository<Recurso, Long>{

	public List<Recurso> findByStatus(Status status);
	
	public Optional<Recurso> findByCodigo(String codigo);
	
	@Query(value = "Select r from Recurso r where "
			+ "(lower(r.codigo) like lower(concat('%',?1,'%')) or lower(r.nome) like lower(concat('%',?1,'%'))) "
			+ "and r.status = com.faculdadecdl.pi3.entity.enums.Status.ATIVO")
	public List<Recurso> findByCodigoOrNomeAndIsActive(String filtro);
	
}

package com.faculdadecdl.pi3.business.exception;

public class AgendamentoBusinessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -798519050258369293L;

	public AgendamentoBusinessException(String msg) {
		super(msg);
	}
}

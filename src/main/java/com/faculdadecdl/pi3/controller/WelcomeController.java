package com.faculdadecdl.pi3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/app")
public class WelcomeController {


	@GetMapping("")
	public String welcome(@RequestParam(name = "name", defaultValue = "Anonimous") String name, Model model) {
		model.addAttribute("title", "Home");
		return "welcome";
	}

	
}

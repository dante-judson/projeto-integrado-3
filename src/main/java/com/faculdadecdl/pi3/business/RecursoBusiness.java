package com.faculdadecdl.pi3.business;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.faculdadecdl.pi3.business.exception.RecursoBusinessException;
import com.faculdadecdl.pi3.entity.Recurso;
import com.faculdadecdl.pi3.repository.RecursoRepository;

@Component
public class RecursoBusiness {

	@Autowired
	private RecursoRepository recursoRepository;
	
	public List<Recurso> findAll() {
		return this.recursoRepository.findAll();
	}

	public Recurso save(Recurso recurso) throws RecursoBusinessException{
		this.validaUniqueConstraints(recurso);
		return this.recursoRepository.save(recurso);
	}
	
	private void validaUniqueConstraints(Recurso recurso) throws RecursoBusinessException{
		this.recursoRepository.findByCodigo(recurso.getCodigo())
		.ifPresent(value -> {throw new RecursoBusinessException("Código já cadastrado na base de dados, favor utilizar outro código");});
	}
	
	public Recurso findById(Long id) {
		Optional<Recurso> recurso = this.recursoRepository.findById(id);
		recurso.orElseThrow(() -> new RecursoBusinessException("Recurso não encontrado"));
		
		return recurso.get();
	}
	
	public Recurso edit(Recurso recurso) {
		return this.recursoRepository.save(recurso);
	}
	
	public void deleteRecursoById(Long id) {
		Recurso recurso = this.findById(id);
		this.recursoRepository.delete(recurso);
	}
	
	public List<Recurso> buscaPorCodigoOuNome(String filtro) {
		return this.recursoRepository.findByCodigoOrNomeAndIsActive(filtro);
	}
	
}

package com.faculdadecdl.pi3.entity.enums;

public enum TipoDeFuncionario {
	PROFESSOR("Professor",'p'),
	COORDENADOR("Coordenador",'c'),
	FUNCIONARIO("Funcionário",'f');
	
	public String displayValue;
	public char databaseValue;
	
	TipoDeFuncionario(String displayValue, char databaseValue) {
		this.displayValue = displayValue;
		this.databaseValue = databaseValue;
	}
	
	public TipoDeFuncionario getEnumByDatabseValue(char databaseValue) {
		TipoDeFuncionario[] tipos = TipoDeFuncionario.values();
		for(TipoDeFuncionario tipo : tipos) {
			if (tipo.databaseValue == databaseValue) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoDeFuncionario.class,"databaseValue");
	}
}

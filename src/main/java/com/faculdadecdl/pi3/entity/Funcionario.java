package com.faculdadecdl.pi3.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.faculdadecdl.pi3.entity.enums.Status;
import com.faculdadecdl.pi3.entity.enums.TipoDeFuncionario;

@Entity
public class Funcionario {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotEmpty(message = "O campo CPF não pode ser vazio")
	@Size(max = 14, min = 14, message = "CPF com formato inválido")
	@Column(unique = true,nullable = false)
	private String cpf;

	@NotEmpty(message = "O campo Nome não pode ser vazio")
	@Column(nullable = false)
	private String nomeCompleto;

	@NotEmpty(message = "O campo Email não pode ser vazio")
	@Email(message = "Email com formato inválido")
	@Column(unique = true, nullable = false)
	private String email;

	@NotEmpty(message = "O campo Usuario não pode ser vazio")
	@Column(unique = true, nullable = false)
	private String usuario;

	@NotEmpty(message = "O campo Senha não pode ser vazio")
	@Column(nullable = false)
	private String senha;

	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;

	@NotNull
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoDeFuncionario tipoDeFuncionario;
	
	@Transient
	private String confirmarSenha;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public TipoDeFuncionario getTipoDeFuncionario() {
		return tipoDeFuncionario;
	}

	public void setTipoDeFuncionario(TipoDeFuncionario tipoDeFuncionario) {
		this.tipoDeFuncionario = tipoDeFuncionario;
	}
	
	public String getConfirmarSenha() {
		return confirmarSenha;
	}

	public void setConfirmarSenha(String confirmarSenha) {
		this.confirmarSenha = confirmarSenha;
	}

	@Override
	public String toString() {
		return "Funcionario [Id=" + id + ", cpf=" + cpf + ", nomeCompleto=" + nomeCompleto + ", email=" + email
				+ ", usuario=" + usuario + ", status=" + status + ", tipoDeFuncionario=" + tipoDeFuncionario + "]";
	}
	
	public Funcionario() {
	}
	
	public Funcionario(Long id) {
		this.id = id;
	}
}

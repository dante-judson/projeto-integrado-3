package com.faculdadecdl.pi3.business.exception;

public class RelatorioBusinessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 905324109920918548L;
	
	public RelatorioBusinessException(String msg) {
		super(msg);
	}

}
